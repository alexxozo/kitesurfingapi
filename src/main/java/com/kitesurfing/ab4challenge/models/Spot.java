package com.kitesurfing.ab4challenge.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString

@Document(collection = "spots")
public class Spot {
    @Id
    private String _id;
    private String name;
    private double longitude;
    private double latitude;
    private int windProbability;
    private String country;
    private String whenToGo;

    protected Spot() {

    }

    public Spot(String id, String name, double longitude, double latitude, int windProbability, String country, String whenToGo) {
        this._id = id;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.windProbability = windProbability;
        this.country = country;
        this.whenToGo = whenToGo;
    }
}
